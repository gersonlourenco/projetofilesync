/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gerson.filesync.services;

/**
 *
 * @author Gerson
 */
public abstract class ThreadService implements Runnable {

    protected Thread thread;

    public ThreadService() {
        thread = null;
    }

    public Boolean isHabilited() {
        return thread != null;
    }

    public void finalizer() {
        if (thread != null) {
            thread.interrupt();
            thread = null;
        }
    }

    public void startThread() {
        thread = new Thread(this);
        thread.start();
    }

    public void finishThread() {
        thread.interrupt();
        thread = null;
    }

    public void startFinishThread() {
        /* interrompe qualquer execução anterior, se existir */
        if (thread == null) {
            startThread();
        } else {
            finishThread();
        }
    }
}
