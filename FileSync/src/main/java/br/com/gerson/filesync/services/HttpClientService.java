/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gerson.filesync.services;

import br.com.gerson.filesync.util.Configurations;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

/**
 *
 * @author Gerson
 */
public class HttpClientService {
    public Response processDelete(String serverPath) {
        Client client = JerseyClientBuilder.createClient();
        WebTarget webTarget = client.target(Configurations.URL_HOST + serverPath);

        Invocation.Builder invocationBuilder = webTarget.request();
        
        return invocationBuilder.delete();
    }
    
    public Response processJSONGet(String[] paths, String serverPath) {
        Client client = JerseyClientBuilder.createClient();
        WebTarget webTarget = client.target(Configurations.URL_HOST + serverPath);

        for (String path : paths) {
            webTarget = webTarget.path(path);
        }

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        
        return invocationBuilder.get();
    }

    public Response processJSONPost(Object obj, String serverPath) {
        Client client = JerseyClientBuilder.createClient();

        WebTarget webTarget = client.target(Configurations.URL_HOST + serverPath);

        Invocation.Builder invocationBuilder = webTarget.request();

        return invocationBuilder.post(Entity.entity(obj, MediaType.APPLICATION_JSON));
    }

    public synchronized void processMultPartPost(File arqEnv, String serverPath) {
        Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();
        FileDataBodyPart filePart = new FileDataBodyPart("file", arqEnv);
        FormDataMultiPart formDataMultiPart = new FormDataMultiPart();
        FormDataMultiPart multipart = (FormDataMultiPart) formDataMultiPart.field("foo", "bar").bodyPart(filePart);
        WebTarget target = client.target(Configurations.URL_HOST + serverPath);
        Response response = target.request().post(Entity.entity(multipart, multipart.getMediaType()));

        try {
            formDataMultiPart.close();
        } catch (IOException ex) {
            Logger.getLogger(HttpClientService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
