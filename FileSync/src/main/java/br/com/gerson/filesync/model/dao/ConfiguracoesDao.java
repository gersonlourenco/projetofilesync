/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gerson.atualizaArquivosClient.model.dao;

import br.com.gerson.atualizaArquivosClient.model.entity.Configuracoes;
import java.util.List;
import javax.persistence.TypedQuery;

/**
 *
 * @author Gerson
 */
public class ConfiguracoesDao extends GenericDao {

    public List<Configuracoes> findAll() {
        TypedQuery<Configuracoes> query = em.createQuery("SELECT u FROM Configuracoes u",
                Configuracoes.class);

        return query.getResultList();
    }
}
