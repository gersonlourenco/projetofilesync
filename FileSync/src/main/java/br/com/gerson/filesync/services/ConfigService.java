/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gerson.filesync.services;

import br.com.gerson.filesync.util.Configurations;

/**
 *
 * @author Desenvolvimento
 */
public class ConfigService {

    private String diretorySelected;
    private Long timeSync;

    public ConfigService() {
        diretorySelected = Configurations.INITIAL_DIR;
        timeSync = Configurations.INITIAL_TIME_SYNC;
    }

    public String getDiretorySelected() {
        //reiniciar threads
        return diretorySelected;
    }

    public void setDiretorySelected(String dirSelecionado) {
        this.diretorySelected = dirSelecionado;
    }

    public Long getTimeSync() {
        return timeSync;
    }
}
