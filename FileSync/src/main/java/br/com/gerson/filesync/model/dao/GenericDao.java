/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gerson.atualizaArquivosClient.model.dao;

import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.transaction.Transactional;

/**
 *
 * @author Gerson
 */
public abstract class GenericDao implements Serializable {

    /* padrão type = PersistenceContextType.TRANSACTION */
    @PersistenceContext(unitName = "br.com.gerson_atualizaArquivoClient_jar_0.0.1PU")
    protected EntityManager em;

    /* padrão type = Transactional.TxType.REQUIRED */
    @Transactional
    public void create(Object objeto) throws PersistenceException {
        em.persist(objeto);
    }

    @Transactional
    public void update(Object objeto) throws PersistenceException {
        em.merge(objeto);
    }

    @Transactional
    public void delete(Object objeto) throws PersistenceException {
        Object merge = em.merge(objeto);
        em.remove(merge);
    }

    public void commit() throws PersistenceException {
        em.flush();
        em.clear();
    }
}
