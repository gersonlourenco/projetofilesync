/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gerson.filesync.controller;

import br.com.gerson.filesync.services.FileMonitorService;
import br.com.gerson.filesync.services.FileSyncService;

/**
 *
 * @author Gerson
 */
public class MonitorController {

    private final FileMonitorService fileMonitorService;
    private final FileSyncService fileSyncService;

    public MonitorController(FileSyncService fileSyncService, FileMonitorService fileMonitorService) {
        this.fileMonitorService = fileMonitorService;
        this.fileSyncService = fileSyncService;
    }

    public void habilitaDesabilitaMonitor() {
        fileSyncService.startFinishThread();
        fileMonitorService.startFinishThread();
    }

    public Boolean getMonitorHab() {
        return fileMonitorService.isHabilited();
    }


    /*Path dir = Paths.get(configCtrl.getDirSelecionado());

                    try {
                        WatchService watchService = dir.getFileSystem().newWatchService();
                        dir.register(watchService,
                                StandardWatchEventKinds.ENTRY_CREATE,
                                StandardWatchEventKinds.ENTRY_DELETE,
                                StandardWatchEventKinds.ENTRY_MODIFY);

                        WatchKey key;
                        while ((key = watchService.take()) != null) {
                            for (WatchEvent<?> event : key.pollEvents()) {
                                System.out.println(
                                        "Event kind:" + event.kind()
                                        + ". File affected: " + event.context() + ".");
                            }

                            key.reset();
                        }
                    } catch (IOException | InterruptedException ex) {
                        Logger.getLogger(MonitoracaoController.class.getName()).log(Level.SEVERE, null, ex);
                    }*/
}
