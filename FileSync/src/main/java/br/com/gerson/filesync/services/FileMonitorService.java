/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gerson.filesync.services;

import br.com.gerson.filesync.model.ArquivosServer;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Gerson
 */
public class FileMonitorService extends ThreadService {

    private final ConfigService configService;
    private final FileSyncService fileSyncService;

    public FileMonitorService(FileSyncService fileSyncService, ConfigService configService) {
        super();
        this.fileSyncService = fileSyncService;
        this.configService = configService;

    }

    private Map<String, Long> recoverServerStatus() {
        Map<String, Long> filesAntes = new HashMap<>();//BD
        return filesAntes;
    }

    @Override
    public void run() {
        Map<String, Long> filesAntes = recoverServerStatus();
        Map<String, Long> filesAtual;

        while (thread != null && !thread.isInterrupted()) {
            filesAtual = new HashMap<>();
            File dir = new File(configService.getDiretorySelected());
            String[] paths = dir.list();
            for (String f : paths) {
                String ref = configService.getDiretorySelected() + File.separator + f;
                File fTest = new File(ref);
                filesAtual.put(f, fTest.lastModified());
            }

            for (Map.Entry<String, Long> entry : filesAtual.entrySet()) {
                Long lastModif = filesAntes.get(entry.getKey());

                /* Não tem na lista, foi adicionado */
                if (lastModif == null) {
                    ArquivosServer arqAux = new ArquivosServer(entry.getKey(),
                            entry.getValue(), ArquivosServer.ATUALIZAR);
                    fileSyncService.queueAdd(arqAux);
                    System.out.println("Arq add " + entry.getKey());
                    continue;
                    /* Ultima modificação diferente, foi atualizado */
                } else if (!entry.getValue().equals(lastModif)) {
                    ArquivosServer arqAux = new ArquivosServer(entry.getKey(),
                            entry.getValue(), ArquivosServer.ATUALIZAR);
                    fileSyncService.queueAdd(arqAux);
                    System.out.println("Arq Atualizado " + entry.getKey());
                }

                filesAntes.remove(entry.getKey());
            }

            /* O que sobrou, foi removido */
            for (Map.Entry<String, Long> entry : filesAntes.entrySet()) {
                System.out.println("Arq removido " + entry.getKey());
                ArquivosServer arqAux = new ArquivosServer(entry.getKey(),
                        entry.getValue(), ArquivosServer.DELETAR);
                fileSyncService.queueAdd(arqAux);                
            }

            /* Atualiza arquivos atuais */
            filesAntes = filesAtual;

            try {
                Thread.sleep(configService.getTimeSync());
            } catch (InterruptedException ex) {
                //não faz nada, acontece normalmente
            }
        }
    }
}
