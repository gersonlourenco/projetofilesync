/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gerson.filesync.model;

import java.io.Serializable;

/**
 *
 * @author Gerson
 */
public class ArquivosServer implements Serializable {
    public static Long DELETAR = 1l;
    public static Long ATUALIZAR = 2l;
    public static Long NADA = 3l;
    
    private String pathFile;
    private Long lastModification;
    private Long acao;

    public ArquivosServer(String absolutePath, Long lastModification, Long acao) {
        this.pathFile = absolutePath;
        this.lastModification = lastModification;
        this.acao = acao;
    }
    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }   

    public Long getLastModification() {
        return lastModification;
    }

    public void setLastModification(Long lastModification) {
        this.lastModification = lastModification;
    }

    public Long getAcao() {
        return acao;
    }

    public void setAcao(Long acao) {
        this.acao = acao;
    }        
}
