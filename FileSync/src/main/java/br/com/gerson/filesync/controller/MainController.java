package br.com.gerson.filesync.controller;

import br.com.gerson.filesync.services.FileMonitorService;
import br.com.gerson.filesync.services.FileSyncService;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Gerson
 */
public class MainController {

    private FileSyncService fileSyncService;
    private FileMonitorService fileMonitorService;
    //ConfiguracoesDao configDao = new ConfiguracoesDao();

    public MainController() {
        //List<Configuracoes> conf = configDao.findAll();
    }

    public MainController(FileSyncService fileSyncService, FileMonitorService fileMonitorService) {
        this.fileSyncService = fileSyncService;
        this.fileMonitorService = fileMonitorService;
    }

    public void recuperaConfig() {

    }

    public void finishProgram() {
        //finaliza Threads 
        fileMonitorService.finishThread();
        fileSyncService.finishThread();
        fileSyncService = null;
        fileMonitorService = null;        
    }
}
