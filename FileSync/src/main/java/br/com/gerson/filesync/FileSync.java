/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gerson.filesync;


import br.com.gerson.filesync.services.HttpClientService;
import br.com.gerson.filesync.controller.ConfiguracoesController;
import br.com.gerson.filesync.controller.MainController;
import br.com.gerson.filesync.controller.MonitorController;
import br.com.gerson.filesync.services.ConfigService;
import br.com.gerson.filesync.services.FileMonitorService;
import br.com.gerson.filesync.services.FileSyncService;
import br.com.gerson.filesync.view.ConfiguracoesFrame;
import br.com.gerson.filesync.view.MainForm;
import br.com.gerson.filesync.view.MonitoracaoFrame;

/**
 *
 * @author Gerson
 */
public class FileSync {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* Cria serviços */
        HttpClientService httpClient = new HttpClientService();
        ConfigService configService = new ConfigService();
        FileSyncService fileSyncService = new FileSyncService(httpClient);
        FileMonitorService fileMonitorService = new FileMonitorService(fileSyncService, configService);
                
        /* Cria controllers */
        MonitorController monitorCtrl = new MonitorController(fileSyncService, fileMonitorService);
        ConfiguracoesController configCtrl = new ConfiguracoesController(configService);
        MainController mainCtrl = new MainController(fileSyncService, fileMonitorService);

        /* Cria form e frames */                 
        ConfiguracoesFrame configInternFrame = new ConfiguracoesFrame(configCtrl);                               
        MonitoracaoFrame monitoraInternFrame = new MonitoracaoFrame(monitorCtrl);
        /*Map<String, JInternalFrame> mapFrames = new HashMap<>(); 
        mapFrames.put("", configInternFrame);
        mapFrames.put("", monitoraInternFrame);*/
        MainForm mainForm = new MainForm(mainCtrl, configInternFrame, monitoraInternFrame);   
        
        /* Mostra form */
        mainForm.setVisible(true);
    }

}
