/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gerson.atualizaArquivosClient.model.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author Gerson
 */
@Entity
@Table(name = "configuracoes")
public class Configuracoes implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;
    
    @JoinColumn(name="diretorioIni_id", referencedColumnName = "id")
    @OneToOne
    private Diretorio diretorioIni;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Diretorio getDiretorioIni() {
        return diretorioIni;
    }

    public void setDiretorioIni(Diretorio diretorioIni) {
        this.diretorioIni = diretorioIni;
    }        
}
