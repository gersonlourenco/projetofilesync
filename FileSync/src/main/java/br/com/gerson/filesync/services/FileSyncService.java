/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gerson.filesync.services;

import br.com.gerson.filesync.model.ArquivosServer;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author Desenvolvimento
 */
public class FileSyncService extends ThreadService {

    private final HttpClientService httpService;
    private List<ArquivosServer> fileSyncQueue;

    public FileSyncService(HttpClientService httpService) {
        super();
        fileSyncQueue = new ArrayList<>();
        this.httpService = httpService;
    }

    @Override
    public void startFinishThread() {
        if (thread == null) {
            fileSyncQueue = new ArrayList<>();
            super.startThread();
        } else {
            super.finishThread();
        }
    }

    public void queueAdd(ArquivosServer arquivo) {
        fileSyncQueue.add(arquivo);
    }

    @Override
    public void run() {
        while (thread != null && !thread.isInterrupted()) {
            if (!fileSyncQueue.isEmpty()) {
                if (Objects.equals(fileSyncQueue.get(0).getAcao(), ArquivosServer.ATUALIZAR)) {
                    httpService.processMultPartPost(new File(fileSyncQueue.get(0).getPathFile()),
                            "rest/arquivosservice");
                } else {
                    httpService.processDelete("rest/arquivosservice/"+fileSyncQueue.get(0).getPathFile());
                }

                fileSyncQueue.remove(0);
            }
        }
    }
}
