/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.gerson.filesync.controller;

import br.com.gerson.filesync.services.ConfigService;

/**
 *
 * @author Gerson
 */
public class ConfiguracoesController {

    private final ConfigService configService;

    public ConfiguracoesController(ConfigService configService) {
        this.configService = configService;
    }

    public String findDiretorySelected() {
        return configService.getDiretorySelected();
    }

    public void updateDiretorySelected(String dirSelected) {
        configService.setDiretorySelected(dirSelected);
    }
}
