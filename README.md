# ProjetoFileSync
## 1 - FileSyncServer
Deve ser iniciado para que que o cliente Java possa funcionar. 
. Após isso basta fazer deploy do projeto no glassfish instalado.

## 1 - FileSync
Cliente Java Desktop responsável por sincronizar os arquivos com o servidor. 

Antes de rodá-lo é necessário configurar o endereço do servidor, na classe 
Configurations do pacote br.com.gerson.util procure a variável URL_HOST 
e altere-a para o endereço do host que foi feito deploy acima.

Para para iniciar o monitoramento, primeiro entre no menu File->Configurações e escolher a pasta a ser monitorada. 
Depois vá no menu File->Monitoramento e iniciar o monitoramento.
