package com.gerson.filesyncserver.controller;

import com.gerson.filesyncserver.model.Arquivos;
import com.gerson.filesyncserver.service.ArquivosService;
import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.gson.Gson;
import java.util.List;
import javax.inject.Inject;

@Path("/arquivosservice")
public class ArquivoController {

    @Inject
    private ArquivosService arqService;
    private final Gson gson;

    public ArquivoController() {
        gson = new Gson();
    }

    /*
    Get    -> recuperar
    post   -> criar
    delete -> deletar
    put    -> atualizar
     */
    @Path("{filePath}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getFile(@PathParam("filePath") String filePath) {
        return gson.toJson(arqService.buscaArquivo(filePath));
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getFiles() {
        System.out.println("getArquivo -> ");
        List<Arquivos> list = arqService.buscaArquivos();

        return gson.toJson(list);
    }

    @POST
    @Consumes({MediaType.MULTIPART_FORM_DATA})
    public Response uploadFile(@FormDataParam("file") InputStream fileInputStream,
            @FormDataParam("file") FormDataContentDisposition fileMetaData) throws Exception {
        try {
            arqService.uploadArquivo(fileInputStream, fileMetaData.getFileName());
        } catch (IOException e) {
            throw new WebApplicationException("Error while uploading file. Please try again !!");
        }
        
        return Response.ok("Data uploaded successfully !!").build();
    }

    @DELETE
    @Path("/{filePath}")
    public void deleteFile(final @PathParam("filePath") String filePath) {
        try {
            arqService.deletaArquivo(filePath);
        } catch (IOException e) {
            throw new WebApplicationException("Error while uploading file. Please try again !!");
        }
    }

    /*@PUT
    @Consumes("application/json")
    public void putPalestra(String filePath) {        
        System.out.println("AtualizA arq " + filePath);
    }*/
}
