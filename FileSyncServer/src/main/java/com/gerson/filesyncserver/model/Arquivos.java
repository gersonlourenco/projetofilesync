/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gerson.filesyncserver.model;

/**
 *
 * @author Gerson
 */
public class Arquivos {
    private String absolutePath;
    private Long lastModification;

    public Arquivos(String absolutePath, Long lastModification) {
        this.absolutePath = absolutePath;
        this.lastModification = lastModification;
    }
    public String getAbsolutePath() {
        return absolutePath;
    }

    public void setAbsolutePath(String absolutePath) {
        this.absolutePath = absolutePath;
    }   

    public Long getLastModification() {
        return lastModification;
    }

    public void setLastModification(Long lastModification) {
        this.lastModification = lastModification;
    }
    
    
}
