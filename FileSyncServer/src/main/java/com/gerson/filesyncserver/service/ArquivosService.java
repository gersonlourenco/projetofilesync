package com.gerson.filesyncserver.service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.gerson.filesyncserver.model.Arquivos;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Gerson
 */
public class ArquivosService {

    private final String UPLOAD_PATH = "c:/temporario/";

    public ArquivosService() {
        File file = new File(UPLOAD_PATH);
        file.mkdirs();
    }        

    private List<Arquivos> listaArq(File file) throws NullPointerException {
        List<Arquivos> listFiles = new ArrayList<>();

        if (file.exists()) {
            for (File fileAux : file.listFiles()) {
                if (fileAux.isDirectory()) {
                    listFiles.addAll(listaArq(fileAux));
                } else {
                    Arquivos arq = new Arquivos(fileAux.getAbsolutePath(),
                            fileAux.lastModified());
                    listFiles.add(arq);
                }
            }
        }

        return listFiles;
    }

    public void uploadArquivo(InputStream fileInputStream, String fileName) throws IOException {
        System.out.println("uploadFile -> ");

        byte[] bytes = new byte[1024];

        File outFile = new File(UPLOAD_PATH + fileName);
        // outFile.mkdirs();
        OutputStream outStream = new FileOutputStream(outFile);
        while (fileInputStream.available() > 0) {
            outStream.write(bytes, 0, fileInputStream.read(bytes));
        }

        outStream.flush();
        outStream.close();
    }

    public void deletaArquivo(String filePath) throws IOException {
        System.out.println("deleta arq " + filePath);

        File file = new File(UPLOAD_PATH + filePath);
        file.delete();
    }

    public List<Arquivos> buscaArquivos() {
        System.out.println("getArquivos -> ");

        return listaArq(new File(UPLOAD_PATH));
    }

    public Arquivos buscaArquivo(String fileName) throws NullPointerException {
        System.out.println("getArquivo -> ");

        Arquivos arq = null;
        File file = new File(UPLOAD_PATH + fileName);
        
        if (file.exists()) {
            arq = new Arquivos(file.getAbsolutePath(), file.lastModified());
        }

        return arq;
    }
}
